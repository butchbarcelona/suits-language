/* Generated By:JJTree: Do not edit this line. DirectAbstractDeclarator.java Version 6.0 */
/* JavaCCOptions:MULTI=true,NODE_USES_PARSER=true,VISITOR=true,TRACK_TOKENS=true,NODE_PREFIX=,NODE_EXTENDS=,NODE_FACTORY=,SUPPORT_CLASS_VISIBILITY_PUBLIC=true */
public
class DirectAbstractDeclarator extends SimpleNode {
  public DirectAbstractDeclarator(int id) {
    super(id);
  }

  public DirectAbstractDeclarator(Suits p, int id) {
    super(p, id);
  }


  /** Accept the visitor. **/
  public Object jjtAccept(SuitsVisitor visitor, Object data) {

    return
    visitor.visit(this, data);
  }
}
/* JavaCC - OriginalChecksum=3654e3a810580e41cd153168da23fa91 (do not edit this line) */
