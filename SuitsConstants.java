/* Generated By:JJTree&JavaCC: Do not edit this line. SuitsConstants.java */

/**
 * Token literal values and constants.
 * Generated by org.javacc.parser.OtherFilesGen#start()
 */
public interface SuitsConstants {

  /** End of File. */
  int EOF = 0;
  /** RegularExpression Id. */
  int INTEGER_LITERAL = 12;
  /** RegularExpression Id. */
  int DECIMAL_LITERAL = 13;
  /** RegularExpression Id. */
  int HEX_LITERAL = 14;
  /** RegularExpression Id. */
  int OCTAL_LITERAL = 15;
  /** RegularExpression Id. */
  int FLOATING_POINT_LITERAL = 16;
  /** RegularExpression Id. */
  int EXPONENT = 17;
  /** RegularExpression Id. */
  int CHARACTER_LITERAL = 18;
  /** RegularExpression Id. */
  int STRING_LITERAL = 19;
  /** RegularExpression Id. */
  int SEMICOLON = 20;
  /** RegularExpression Id. */
  int MAIN = 21;
  /** RegularExpression Id. */
  int VOID = 22;
  /** RegularExpression Id. */
  int CHAR = 23;
  /** RegularExpression Id. */
  int INT = 24;
  /** RegularExpression Id. */
  int DOUBLE = 25;
  /** RegularExpression Id. */
  int FLOAT = 26;
  /** RegularExpression Id. */
  int SHORT = 27;
  /** RegularExpression Id. */
  int LONG = 28;
  /** RegularExpression Id. */
  int STRING = 29;
  /** RegularExpression Id. */
  int DFLT = 30;
  /** RegularExpression Id. */
  int CONTINUE = 31;
  /** RegularExpression Id. */
  int BREAK = 32;
  /** RegularExpression Id. */
  int WHILE = 33;
  /** RegularExpression Id. */
  int FOR = 34;
  /** RegularExpression Id. */
  int IF = 35;
  /** RegularExpression Id. */
  int ELSE = 36;
  /** RegularExpression Id. */
  int CASE = 37;
  /** RegularExpression Id. */
  int RETURN = 38;
  /** RegularExpression Id. */
  int SWITCH = 39;
  /** RegularExpression Id. */
  int DO = 40;
  /** RegularExpression Id. */
  int VOLATILE = 41;
  /** RegularExpression Id. */
  int REGISTER = 42;
  /** RegularExpression Id. */
  int UNSIGNED = 43;
  /** RegularExpression Id. */
  int TYPEDEF = 44;
  /** RegularExpression Id. */
  int SIZEOF = 45;
  /** RegularExpression Id. */
  int EXTERN = 46;
  /** RegularExpression Id. */
  int STRUCT = 47;
  /** RegularExpression Id. */
  int STATIC = 48;
  /** RegularExpression Id. */
  int SIGNED = 49;
  /** RegularExpression Id. */
  int UNION = 50;
  /** RegularExpression Id. */
  int CONST = 51;
  /** RegularExpression Id. */
  int ENUM = 52;
  /** RegularExpression Id. */
  int AUTO = 53;
  /** RegularExpression Id. */
  int GOTO = 54;
  /** RegularExpression Id. */
  int PRINT = 55;
  /** RegularExpression Id. */
  int PRINT_NL = 56;
  /** RegularExpression Id. */
  int IDENTIFIER = 57;
  /** RegularExpression Id. */
  int LETTER = 58;
  /** RegularExpression Id. */
  int DIGIT = 59;

  /** Lexical state. */
  int DEFAULT = 0;
  /** Lexical state. */
  int PREPROCESSOR_OUTPUT = 1;

  /** Literal token values. */
  String[] tokenImage = {
    "<EOF>",
    "\" \"",
    "\"\\t\"",
    "\"\\n\"",
    "\"\\r\"",
    "<token of kind 5>",
    "<token of kind 6>",
    "\"#\"",
    "\"\\n\"",
    "\"\\\\\\n\"",
    "\"\\\\\\r\\n\"",
    "<token of kind 11>",
    "<INTEGER_LITERAL>",
    "<DECIMAL_LITERAL>",
    "<HEX_LITERAL>",
    "<OCTAL_LITERAL>",
    "<FLOATING_POINT_LITERAL>",
    "<EXPONENT>",
    "<CHARACTER_LITERAL>",
    "<STRING_LITERAL>",
    "\";\"",
    "\"law\"",
    "\"abscond\"",
    "\"char\"",
    "\"int\"",
    "\"double\"",
    "\"float\"",
    "\"short\"",
    "\"long\"",
    "\"String\"",
    "\"default\"",
    "\"motion\"",
    "\"bail\"",
    "\"injunction\"",
    "\"for\"",
    "\"plea\"",
    "\"bargain\"",
    "\"suit\"",
    "\"carryback\"",
    "\"reversal\"",
    "\"execute\"",
    "\"volatile\"",
    "\"register\"",
    "\"unsigned\"",
    "\"typedef\"",
    "\"sizeof\"",
    "\"extern\"",
    "\"struct\"",
    "\"static\"",
    "\"signed\"",
    "\"union\"",
    "\"const\"",
    "\"enum\"",
    "\"auto\"",
    "\"goto\"",
    "\"print\"",
    "\"println\"",
    "<IDENTIFIER>",
    "<LETTER>",
    "<DIGIT>",
    "\"{\"",
    "\"}\"",
    "\",\"",
    "\"=\"",
    "\":\"",
    "\"(\"",
    "\")\"",
    "\"[\"",
    "\"]\"",
    "\"*\"",
    "\"...\"",
    "\"*=\"",
    "\"/=\"",
    "\"%=\"",
    "\"+=\"",
    "\"-=\"",
    "\"?\"",
    "\"||\"",
    "\"&&\"",
    "\"==\"",
    "\"!=\"",
    "\"<\"",
    "\">\"",
    "\"<=\"",
    "\">=\"",
    "\"+\"",
    "\"-\"",
    "\"/\"",
    "\"%\"",
    "\"++\"",
    "\"--\"",
    "\"&\"",
    "\"~\"",
    "\"!\"",
    "\".\"",
    "\"->\"",
  };

}
