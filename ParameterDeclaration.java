/* Generated By:JJTree: Do not edit this line. ParameterDeclaration.java Version 6.0 */
/* JavaCCOptions:MULTI=true,NODE_USES_PARSER=true,VISITOR=true,TRACK_TOKENS=true,NODE_PREFIX=,NODE_EXTENDS=,NODE_FACTORY=,SUPPORT_CLASS_VISIBILITY_PUBLIC=true */
public
class ParameterDeclaration extends SimpleNode {
  public ParameterDeclaration(int id) {
    super(id);
  }

  public ParameterDeclaration(Suits p, int id) {
    super(p, id);
  }


  /** Accept the visitor. **/
  public Object jjtAccept(SuitsVisitor visitor, Object data) {

    return
    visitor.visit(this, data);
  }
}
/* JavaCC - OriginalChecksum=a6f72275f261fb58e06e575f01379d3c (do not edit this line) */
