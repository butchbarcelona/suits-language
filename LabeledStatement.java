/* Generated By:JJTree: Do not edit this line. LabeledStatement.java Version 6.0 */
/* JavaCCOptions:MULTI=true,NODE_USES_PARSER=true,VISITOR=true,TRACK_TOKENS=true,NODE_PREFIX=,NODE_EXTENDS=,NODE_FACTORY=,SUPPORT_CLASS_VISIBILITY_PUBLIC=true */
public
class LabeledStatement extends SimpleNode {
  public LabeledStatement(int id) {
    super(id);
  }

  public LabeledStatement(Suits p, int id) {
    super(p, id);
  }


  /** Accept the visitor. **/
  public Object jjtAccept(SuitsVisitor visitor, Object data) {

    return
    visitor.visit(this, data);
  }
}
/* JavaCC - OriginalChecksum=efb01653b29b78eb717352db79d496c1 (do not edit this line) */
