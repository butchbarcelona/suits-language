/* Generated By:JJTree: Do not edit this line. DeclarationList.java Version 6.0 */
/* JavaCCOptions:MULTI=true,NODE_USES_PARSER=true,VISITOR=true,TRACK_TOKENS=true,NODE_PREFIX=,NODE_EXTENDS=,NODE_FACTORY=,SUPPORT_CLASS_VISIBILITY_PUBLIC=true */
public
class DeclarationList extends SimpleNode {
  public DeclarationList(int id) {
    super(id);
  }

  public DeclarationList(Suits p, int id) {
    super(p, id);
  }


  /** Accept the visitor. **/
  public Object jjtAccept(SuitsVisitor visitor, Object data) {

    return
    visitor.visit(this, data);
  }
}
/* JavaCC - OriginalChecksum=22cda7aecb49284f10fcfbce94ae6982 (do not edit this line) */
