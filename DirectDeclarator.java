/* Generated By:JJTree: Do not edit this line. DirectDeclarator.java Version 6.0 */
/* JavaCCOptions:MULTI=true,NODE_USES_PARSER=true,VISITOR=true,TRACK_TOKENS=true,NODE_PREFIX=,NODE_EXTENDS=,NODE_FACTORY=,SUPPORT_CLASS_VISIBILITY_PUBLIC=true */
public
class DirectDeclarator extends SimpleNode {
  String name; 

  public DirectDeclarator(int id) {
    super(id);
  }

  public DirectDeclarator(Suits p, int id) {
    super(p, id);
  }


  /** Accept the visitor. **/
  public Object jjtAccept(SuitsVisitor visitor, Object data) {
    System.out.println(" name:"+name);
    return
    visitor.visit(this, data);
  }
}
/* JavaCC - OriginalChecksum=e0e9c0377827eb277aadaf669a3def5f (do not edit this line) */
