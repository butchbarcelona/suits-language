/* Generated By:JJTree: Do not edit this line. TypeSpecifier.java Version 6.0 */
/* JavaCCOptions:MULTI=true,NODE_USES_PARSER=true,VISITOR=true,TRACK_TOKENS=true,NODE_PREFIX=,NODE_EXTENDS=,NODE_FACTORY=,SUPPORT_CLASS_VISIBILITY_PUBLIC=true */
public
class TypeSpecifier extends SimpleNode {
  public TypeSpecifier(int id) {
    super(id);
  }

  public TypeSpecifier(Suits p, int id) {
    super(p, id);
  }


  /** Accept the visitor. **/
  public Object jjtAccept(SuitsVisitor visitor, Object data) {
 
    return
    visitor.visit(this, data);
  }
}
/* JavaCC - OriginalChecksum=11580ee7a23cd784cdd2f2341e9e9242 (do not edit this line) */
